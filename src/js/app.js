import React, {Component} from 'react';
import {render} from 'react-dom';

import {ExchangeView} from './view/exchange-view';

import './../styles/app.scss';

class App extends Component {
  render() {
    return (
      <div className="app">
        <ExchangeView/>
      </div>
    )
  }
}

render(<App/>, document.getElementById('app')); 
