import React, {Component} from 'react';
import {observer} from 'mobx-react';
import Slider from 'react-slick';

import loader from './../../img/loader.svg';

import {parseAmount} from './../utils/amount';
import {user} from './../storages/user';
import {ExchangeModel} from './../models/exchange-model';

class AmountField extends Component {
  static propTypes = {
    currency: React.PropTypes.string,
    amount: React.PropTypes.string,
    rate: React.PropTypes.string,
    balance: React.PropTypes.string,
    prefix: React.PropTypes.string,
    onChange: React.PropTypes.func,
    balanceWarning: React.PropTypes.bool
  };

  static defaultProps = {
    currency: '',
    amount: '',
    rate: '',
    balance: '',
    prefix: '',
    onChange: () => null
  };

  onChange(val) {
    this.props.onChange(
      this.props.prefix && parseAmount(this.props.amount) > 0 ? val.substr(this.props.prefix.length) : val
    );
  }

  getDecoratedValue() {
    let floatAmount = parseAmount(this.props.amount);

    if (floatAmount > 0) {
      return this.props.prefix + this.props.amount;
    } else {
      return this.props.amount;
    }
  }

  render() {
    return (
      <div className="amount">
        <div className="amount__field">
          <div className="amount__currency">{this.props.currency}</div>
          <div className="amount__wrapper">
            <input
              className="amount__input"
              value={this.getDecoratedValue()}
              onChange={({target: {value}}) => this.onChange(value)}
            />
          </div>
        </div>
        <div className="amount__footer">
          <div className={"amount__balance" + (this.props.balanceWarning ? ' amount__balance--warning' : '')}>
            {`You have ${this.props.balance}`}
          </div>
          <div className="amount__rate">
            {this.props.rate || ''}
          </div>
        </div>
      </div>
    )
  }
}

@observer
class AmountFieldFrom extends Component {
  static propTypes = {
    currency: React.PropTypes.string,
    model: React.PropTypes.instanceOf(ExchangeModel)
  };

  render() {
    return (
      <AmountField
        onChange={(val) => this.props.model.setAmountFrom(val)}
        amount={this.props.model.calcAmountFrom}
        currency={this.props.currency}
        balance={user.getBalanceString(this.props.currency)}
        balanceWarning={!this.props.model.canConvert && this.props.model.amountToConvert > 0}
        prefix="-"
      />
    )
  }
}

@observer
class AmountFieldTo extends Component {
  static propTypes = {
    currency: React.PropTypes.string,
    model: React.PropTypes.instanceOf(ExchangeModel)
  };

  render() {
    return (
      <AmountField
        onChange={(val) => this.props.model.setAmountTo(val)}
        amount={this.props.model.calcAmountTo}
        currency={this.props.currency}
        balance={user.getBalanceString(this.props.currency)}
        rate={this.props.model.rate}
        prefix="+"
      />
    )
  }
}

@observer
class Convert extends Component {
  render() {
    return (
      <div className="convert">
        <button
          className={"convert__button" + (this.props.model.canConvert ? ' convert__button--active' : '')}
          onClick={() => this.props.model.convert()}>
          convert
        </button>
      </div>
    )
  }
}

@observer
export class ExchangeView extends Component {
  model = null;
  sliderSettings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };

  componentWillMount() {
    this.model = new ExchangeModel();
  }

  render() {
    return (
      <div className="exchange">
        <div className={"exchange__loader" + (!this.model.isUpdating ? ' exchange__loader--disabled' : '')}>
          <img src={loader}/>
        </div>
        <Convert model={this.model}/>
        <div className="exchange__content">
          <div className="exchange__from">
            <div className="exchange__wrapper-from">
              <Slider
                {...this.sliderSettings}
                initialSlide={this.model.availableCurrencies.findIndex((itm) => itm === this.model.defaultCurrencies.from)}
                beforeChange={(prevVal, val) => this.model.setCurrencyFrom(this.model.availableCurrencies[val])}>
                {this.model.availableCurrencies.map((currency, i) => (
                  <div>
                    <AmountFieldFrom
                      model={this.model}
                      currency={currency}
                    />
                  </div>
                ))}
              </Slider>
            </div>
          </div>
          <div className="exchange__to">
            <div className="exchange__wrapper-to">
              <Slider
                {...this.sliderSettings}
                initialSlide={this.model.availableCurrencies.findIndex((itm) => itm === this.model.defaultCurrencies.to)}
                beforeChange={(prevVal, val) => this.model.setCurrencyTo(this.model.availableCurrencies[val])}>
                {this.model.availableCurrencies.map((currency) => (
                  <div>
                    <AmountFieldTo
                      model={this.model}
                      currency={currency}
                    />
                  </div>
                ))}
              </Slider>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
