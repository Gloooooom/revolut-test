import {observable, computed, action} from 'mobx';

import {rates} from './../storages/rates';
import {user} from './../storages/user';

import {normalizeAmount, parseAmount, trimAmount, clearZero, roundTo} from './../utils/amount';
import symbols from './../utils/symbols';


export class ExchangeModel {
  constructor() {
    Promise.all([rates.updateRates(), user.updateBalance()])
      .then(() => {
        rates.startUpdating();
        this.isUpdating = false;
      });
  }

  availableCurrencies = ['USD', 'GBP', 'EUR'];

  defaultCurrencies = {
    from: 'USD',
    to: 'EUR'
  };

  @observable currencyFrom = this.defaultCurrencies.from;
  @observable currencyTo = this.defaultCurrencies.to;

  @observable amountFrom = '0';
  @observable amountTo = null;

  @observable isUpdating = true;

  @computed get calcAmountFrom() {
    return this.amountTo != null ? normalizeAmount(
        rates.convert(
          parseAmount(this.amountTo),
          this.currencyTo, this.currencyFrom
        )
      ) : this.amountFrom;
  }

  @computed get calcAmountTo() {
    return this.amountFrom != null ? normalizeAmount(
        rates.convert(
          parseAmount(this.amountFrom),
          this.currencyFrom, this.currencyTo
        )
      ) : this.amountTo;
  }

  @computed get canConvert() {
    let amountToConvert = this.amountToConvert;

    return amountToConvert > 0 && (user.getBalance(this.currencyFrom) >= amountToConvert);
  }

  @computed get rate() {
    return `${symbols(this.currencyFrom)} 1 = ` +
      `${symbols(this.currencyTo)} ${roundTo(rates.getRate(this.currencyFrom, this.currencyTo), 4)}`;
  }

  @computed get amountToConvert() {
    return parseAmount(this.amountTo == null ? this.amountFrom : this.calcAmountFrom);
  }

  @action setAmountFrom(val) {
    if (!this.isUpdating) {
      this.amountFrom = trimAmount(clearZero(this.amountFrom, val));
      this.amountTo = null;
    }
  }

  @action setAmountTo(val) {
    if (!this.isUpdating) {
      this.amountTo = trimAmount(clearZero(this.amountTo, val));
      this.amountFrom = null;
    }
  }

  @action setCurrencyFrom(val) {
    // Не уверен, нужна ли эта функциональность, т.к. с округлением есть некоторые проблемы.
    if (this.amountFrom != null) {
      this.setAmountFrom(trimAmount(rates.convert(this.amountFrom, this.currencyFrom, val)));
    }

    this.currencyFrom = val;
  }

  @action setCurrencyTo(val) {
    this.currencyTo = val;
  }

  @action convert() {
    if (this.canConvert) {
      this.isUpdating = true;

      user.convert({
        currencyFrom: this.currencyFrom,
        currencyTo: this.currencyTo,
        amountFrom: this.amountToConvert
      }).then(() => {
        this.isUpdating = false;
        this.setAmountFrom('0');
      });
    }
  }
}
