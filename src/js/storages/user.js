import {observable} from 'mobx';

import {normalizeAmount} from './../utils/amount';
import symbols from './../utils/symbols';
import api from './../utils/api';

class User {
  constructor() {
    this.updateBalance();
  }

  @observable balance = {};

  updateBalance() {
    return new Promise((resolve, reject) =>
      api.user.balance()
        .then((data) => {
          this.balance = data;
          resolve();
        })
        .catch(() => reject(new Error('Cannot update user balance')))
    );
  }

  convert(transaction) {
    return new Promise((resolve, reject) =>
      api.user.convert(transaction)
        .then((data) => {
          this.balance = data;
          resolve(data);
        })
        .catch(reject));
  }

  getBalanceString(currency) {
    return `${symbols(currency)}${normalizeAmount(this.balance[currency])}`;
  }

  getBalance(currency) {
    return this.balance[currency] || 0;
  }
}

export const user = new User();
