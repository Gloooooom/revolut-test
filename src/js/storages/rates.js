import {observable} from 'mobx';

import api from './../utils/api';

class Rates {
  constructor() {
  }

  interval = null;
  delay = 30000;

  @observable data = {};

  startUpdating() {
    this.interval = setInterval(
      () => this.updateRates(),
      this.delay
    );
  }

  updateRates() {
    return new Promise((resolve, reject) =>
      api.rates.latest()
        .then(({rates}) => {
          this.data = rates;
          resolve();
        })
        .catch(() => {
          reject(new Error('Cannot update rates'));
        }));
  }

  stopUpdating() {
    clearInterval(this.interval);
  }

  convert(amount, currencyFrom, currencyTo) {
    return (amount / this.data[currencyFrom]) * this.data[currencyTo];
  }

  getRate(currencyFrom, currencyTo) {
    if (this.data[currencyTo] != null && this.data[currencyFrom] != null) {
      return this.data[currencyTo] / this.data[currencyFrom];
    } else {
      return 1;
    }
  }
}

export const rates = new Rates();
