import {rates} from './../storages/rates';

const APP_ID = "13d38f25d2b74219bb1d3e0d87a51495";
const EXCHANGE_API_URL = "https://openexchangerates.org/api/";

const userBalanceStub = {
  "GBP": 100.2413,
  "EUR": 200.342,
  "USD": 0.561234
};

const api = (() => {
  function parseParams(data) {
    return '?' + Object.keys(data).reduce((res, key) => res + `${res ? '&' : ''}${key}=${data[key]}`, '');
  }

  function makeRequest(api, url, additionalData, type = 'get') {
    return (data) => new Promise((resolve, reject) => {
        const dataWithAppId = {
          ...additionalData,
          ...data
        };

        fetch(
          `${api}${url}${parseParams(dataWithAppId)}`,
          {
            method: type,
            mode: 'cors',
            cache: 'default'
          })
          .then((resp) => resp.json().then((data) => resolve(data)))
          .catch((error) => reject(error));
      },
    );
  }

  function makeRequestToExchange(url, type) {
    return makeRequest(EXCHANGE_API_URL, url, {"app_id": APP_ID}, type);
  }

  function makeStub(cb) {
    return (data) => new Promise((resolve) => {
      setTimeout(() => resolve(cb(data)), Math.floor(Math.random() * 200 + 100));
    });
  }

  return {
    rates: {
      latest: makeRequestToExchange('latest.json')
    },
    user: {
      balance: makeStub(() => Object.assign({}, userBalanceStub)),
      convert: makeStub((data) => {
        userBalanceStub[data.currencyFrom] -= data.amountFrom;
        userBalanceStub[data.currencyTo] += rates.convert(
          data.amountFrom,
          data.currencyFrom,
          data.currencyTo
        );

        return Object.assign({}, userBalanceStub);
      })
    }
  };
})();

export default api;
