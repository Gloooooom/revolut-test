const amountRegExp = /^(0|(\d+))(\.\d{0,2})?/g;
const zeroRegExp = /[1-9]|(0\.)/g;

export function roundTo(number, discharge) {
  let dischargeMultiplier = 10 ** discharge;

  return Math.round(number * dischargeMultiplier) / dischargeMultiplier;
}

export function parseAmount(val) {
  return Math.abs(Number.parseFloat(val)) || 0;
}

export function clearZero(prevVal, nextVal) {
  let clearedValue = (nextVal.match(zeroRegExp) || ['0'])[0];

  return prevVal === '0' ? clearedValue : nextVal;
}

export function trimAmount(val) {
  return (val.toString().match(amountRegExp) || ['0'])[0];
}

export function normalizeAmount(val) {
  return val !== 0 ? trimAmount(roundTo(val, 2)) : '0';
}
