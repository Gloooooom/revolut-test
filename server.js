'use strict'

var express = require('express');
var path = require('path');

var app = express();

app.use('/', express.static(path.resolve(__dirname + '/build')));

app.listen(8888, () => {
  console.log('client on *:8888');
});
