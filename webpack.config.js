var path = require("path");
var precss = require('precss');
var autoprefixer = require('autoprefixer');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');
var webpack = require('webpack');

module.exports = function (env) {
  const isProd = env && env.prod;

  console.log(isProd ? 'Production build' : 'Development build');

  const plugins = [
    new HtmlWebpackPlugin({
      title: 'Revolut test',
      template: './src/index.html',
      inject: 'body'
    })
  ];

  if (isProd) {
    plugins.push(
      new webpack.optimize.UglifyJsPlugin({
        mangle: false
      }));

    plugins.unshift(
      new CleanWebpackPlugin(['build'], {
        root: path.resolve(__dirname, './'),
        verbose: true,
        dry: false
      }));
  }

  let entryPoints = ['babel-polyfill', path.resolve(__dirname, './src/js/app.js')];

  if (!isProd) {
    entryPoints.unshift("webpack-dev-server/client?http://localhost:8080/");
  }

  let config = {
    entry: entryPoints,
    output: {
      filename: 'js/app.js',
      path: path.resolve(__dirname, "build"),
      publicPath: "",
      pathinfo: true
    },
    devServer: {
      contentBase: path.resolve(__dirname, "build"),
      inline: !isProd
    },
    stats: {
      colors: true,
      reasons: true,
      hash: false,
      modulesSort: 'name'
    },
    cache: true,
    module: {
      rules: [{
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: [{
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
            presets: ['react', 'es2015', 'stage-0'],
            plugins: ['add-module-exports', 'transform-decorators-legacy']
          }
        }]
      }, {
        test: /.(png|woff(2)?|eot|ttf|svg|gif)$/,
        use: {loader: 'url-loader?limit=100000'}
      }, {
        test: /\.scss$/,
        use: [
          {loader: "style-loader"},
          {loader: "css-loader" + (isProd ? '?sourceMap' : '')},
          {
            loader: "postcss-loader",
            options: {
              plugins: () => [precss, autoprefixer]
            }
          },
          {loader: "sass-loader" + (isProd ? '?sourceMap' : '')}
        ]
      }]
    },
    plugins: plugins
  };

  if (!isProd) {
    config.devtool = 'source-map';
  }

  return config;
};
